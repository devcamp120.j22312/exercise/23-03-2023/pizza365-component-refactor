import { Component } from "react";

class Footer extends Component {
    render() {
        return (
            <>
                <div className="div-footer">
                    <div>
                        <h3 >
                            Footer
                        </h3>
                    </div>
                    <div >
                        <button class=" btn btn-dark">To the top</button>
                    </div>
                    <div>
                        <i class="fa fa-facebook-official w3-hover-opacity"></i>
                        <i class="fa fa-instagram w3-hover-opacity"></i>
                        <i class="fa fa-snapchat w3-hover-opacity"></i>
                        <i class="fa fa-pinterest-p w3-hover-opacity"></i>
                        <i class="fa fa-twitter w3-hover-opacity"></i>
                    </div>
                    <div>
                        <p>
                            Powered by DEVCAMP
                        </p>
                    </div>
                </div>
            </>
        )
    }
}

export default Footer;