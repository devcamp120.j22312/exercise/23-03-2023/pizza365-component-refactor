import { Component } from "react";


import logoBrand from "../assets/images/pizza-logo-58188.jpg";
import pizzaCarouselImg from "../assets/images/1.jpg";

class Header extends Component {
    render() {
        return (
            <>
                <div className="div-header">
                    <div className="text-header">
                        Trang Chủ
                    </div>
                    <div className="text-header">
                        Combo
                    </div>
                    <div className="text-header">
                        Loại Pizza
                    </div>
                    <div className="text-header">
                        Gửi Đơn Hàng
                    </div>
                </div>

                <div className="container">
                    <div>
                        <img className="img-brand" src={logoBrand} alt="logo-img" />
                    </div>
                    <div className="div-brand">
                        <h4>PIZZA 365</h4>
                        <p>Truly Italian !</p>
                    </div>

                    <div>
                        <div className="div-img-carousel">
                            <img src={pizzaCarouselImg} slt="First slide" />
                            <div className="div-description">
                                <h5>New Dishes</h5>
                                <p>TUNA PIZZA</p>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
export default Header;