import { Component } from "react";

class DrinkComponent extends Component {
    render() {
        return (
            <>
                <div className="card-header text-center">
                    <h3>CHỌN ĐỒ UỐNG</h3>
                    <hr className="line" />
                </div>
                <div className="card select-drink">
                    <select className="form-control">
                        <option>Chọn nước uống</option>
                        <option>Cocacola</option>
                        <option>Pepsi</option>
                        <option>Aquafina</option>
                        <option>Trà tắc</option>
                    </select>
                </div>
            </>
        )
    }
}

export default DrinkComponent;