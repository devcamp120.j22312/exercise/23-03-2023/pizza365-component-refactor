import { Component } from "react";
import pizzaHawaii from "../../assets/images/hawaiian.jpg";
import pizzaBacon from '../../assets/images/bacon.jpg';
import pizzaSeafood from '../../assets/images/seafood.jpg';

class TypeComponent extends Component {
    render() {
        return (
            <>
                <div className="card-header text-center">
                    <h3>CHỌN LOẠI PIZZA</h3>
                    <hr className="line" />
                </div>
                <div className="div-pizza-select">
                    <div className="card div-card-pizza">
                        <div className="card-header img-header">
                            <img className="div-img" src={pizzaSeafood} alt="pizza seafood" />
                        </div>
                        <div className="card-body">
                            <h3>OCEAN MANIA</h3>
                            <p>PIZZA HẢI SẢN XỐT MAYONNAISE</p>
                            <p>Xốt cà chua, Phô mai Mozzarella, Tôm, Mực, Thanh cua, Hành tây</p>
                        </div>
                        <button className="btn btn-warning btn-choose" id="btn-mania">Chọn</button>
                    </div>
                    <div className="card div-card-pizza">
                        <div className="card-header img-header">
                            <img className="div-img" src={pizzaHawaii} alt="pizza seafood" />
                        </div>
                        <div className="card-body">
                            <h3>HAWAIIAN</h3>
                            <p>PIZZA DĂM BÔNG DỨA KIỂU HAWAII</p>
                            <p>Xốt cà chua, Phô mai Mozzarella, Thịt dăm bông, Thơm</p>
                        </div>
                        <button className="btn btn-warning btn-choose" id="btn-hawaii">Chọn</button>
                    </div>
                    <div className="card div-card-pizza">
                        <div className="card-header img-header">
                            <img className="div-img" src={pizzaBacon} alt="pizza seafood" />
                        </div>
                        <div className="card-body">
                            <h3>BACON</h3>
                            <p>PIZZA BACON</p>
                            <p>Xốt cà chua, Phô mai Mozzarella, Bacon</p>
                        </div>
                        <button className="btn btn-warning btn-choose" id="btn-bacon">Chọn</button>
                    </div>
                </div>
            </>
        )
    }
}

export default TypeComponent;