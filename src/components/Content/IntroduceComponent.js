import { Component } from "react";

class IntroduceComponent extends Component {
    render() {
        return (
            <>
                <div className="card-header text-center">
                    <h3>TẠI SAO LẠI LÀ PIZZA 365</h3>
                    <hr className="line" />
                </div>
                <div className="div-card-deck">
                    <div className="card" style={{ backgroundColor: "lightgoldenrodyellow" }}>
                        <div className="card-body">
                            <h4>Đa dạng</h4>
                            <p>Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot hiện nay </p>
                        </div>
                    </div>
                    <div className="card" style={{ backgroundColor: 'yellow' }}>
                        <div className="card-body">
                            <h4 className="card-title">Chất lượng</h4>
                            <p className="card-text">Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực phẩm.</p>
                        </div>
                    </div>
                    <div className="card" style={{ backgroundColor: "lightsalmon" }}>
                        <div className="card-body">
                            <h4 className="card-title">Hương vị</h4>
                            <p className="card-text">Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải nghiệm từ Pizza365</p>
                        </div>
                    </div>
                    <div className="card" style={{ backgroundColor: 'orange' }}>
                        <div className="card-body">
                            <h4 className="card-title">Dịch vụ</h4>
                            <p className="card-text">Nhân viên thân thiện, nhà hàng hiện đại, dich vụ giao hàng nhanh, chất lượng</p>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default IntroduceComponent;