import { Component } from "react";

class SizeComponent extends Component {
    render() {
        return (
            <>
                <div className="card-header text-center">
                    <h3>CHỌN SIZE PIZZA</h3>
                    <hr className="line" />
                    <p>Chọn combo pizza phù hợp với nhu cầu của bạn.</p>
                </div>
                <div className="div-combo-card-deck">
                    <div className="card div-card-combo">
                        <div className="card-header-combo text-center">
                            <h3>S (small)</h3>
                        </div>
                        <div className="card-body text-center">
                            <p className="card-text">Đường kính: <span className="font-weight-bold"> 20cm</span></p>
                            <hr />
                            <p className="card-text">Sườn nướng: <span className="font-weight-bold"> 2</span></p>
                            <hr />
                            <p className="card-text">Salad: <span className="font-weight-bold"> 200g</span></p>
                            <hr />
                            <p className="card-text">Nước ngọt: <span className="font-weight-bold"> 2</span></p>
                            <hr />
                            <h2>150.000</h2>
                            <p>VNĐ</p>
                        </div>
                        <button className="btn btn-warning btn-choose" id="btn-small">Chọn</button>
                    </div>
                    <div className="card div-card-combo">
                        <div className="card-header-combo text-center">
                            <h3>M (medium)</h3>
                        </div>
                        <div className="card-body text-center">
                            <p className="card-text">Đường kính: <span className="font-weight-bold"> 25cm</span></p>
                            <hr />
                            <p className="card-text">Sườn nướng: <span className="font-weight-bold"> 4</span></p>
                            <hr />
                            <p className="card-text">Salad: <span className="font-weight-bold"> 300g</span></p>
                            <hr />
                            <p className="card-text">Nước ngọt: <span className="font-weight-bold"> 3</span></p>
                            <hr />
                            <h2>200.000</h2>
                            <p>VNĐ</p>
                        </div>
                        <button className="btn btn-warning btn-choose" id="btn-medium">Chọn</button>
                    </div>
                    <div className="card div-card-combo">
                        <div className="card-header-combo text-center">
                            <h3>L (large)</h3>
                        </div>
                        <div className="card-body text-center">
                            <p className="card-text">Đường kính: <span className="font-weight-bold"> 30cm</span></p>
                            <hr />
                            <p className="card-text">Sườn nướng: <span className="font-weight-bold"> 8</span></p>
                            <hr />
                            <p className="card-text">Salad: <span className="font-weight-bold"> 500g</span></p>
                            <hr />
                            <p className="card-text">Nước ngọt: <span className="font-weight-bold"> 4</span></p>
                            <hr />
                            <h2>250.000</h2>
                            <p>VNĐ</p>
                        </div>
                        <button className="btn btn-warning btn-choose" id="btn-large">Chọn</button>
                    </div>
                </div>
            </>
        )
    }
}

export default SizeComponent;