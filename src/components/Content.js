import { Component } from "react";
import DrinkComponent from "./Content/DrinkComponent";
import TypeComponent from "./Content/TypeComponent";
import FormComponent from "./Content/FormComponent";
import SizeComponent from "./Content/SizeComponent";
import IntroduceComponent from "./Content/IntroduceComponent";

class Content extends Component {
    render() {
        return(
            <>
            <IntroduceComponent />
            <SizeComponent />
            <TypeComponent />
            <DrinkComponent />
            <FormComponent />

            </>
        )
    }
}

export default Content;